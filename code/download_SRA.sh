VAR=$(cut -d ',' -f 1 SraRunTable.txt | tail -n +2) # select the first field, containg the IDs needed for the sra-toolkit, and eliminate the header "Run"

for i in ${VAR}
    do
        echo "Download SRA sample: ${i}"
        prefetch -p -O ../dumps/ ${i}                # for each run ID we prefetch the data...
        fasterq-dump -p -O ../dumps/ ../dumps/${i}   # ...and then download it 
    done
